const express = require('express');
const app = express();

app.get('/', (req, res) => {
    res.send('This is a sample for CICD Pipeline');
});

const PORT = 8000;
app.listen(PORT, () => {
    console.log('application is running on port ${PORT}');
});
